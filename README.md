# Next News

### is a web application designed to streamline the process of accessing and interacting with news articles from various sources. The application serves as a centralized hub for news consumption, offering users a convenient and personalized experience.

## Features

- **Server-side Rendering (SSR)**: Leveraging Next.js, Next Level Food implements SSR to enhance performance and SEO, ensuring fast initial page loads and better search engine visibility.
- **Responsive Design**: With Next.js, the application seamlessly adapts to various devices and screen sizes, providing an optimal user experience across desktop, tablet, and mobile.
- **Efficient Routing with App Router**: Utilizing Next.js's built-in App Router, the application ensures efficient routing between pages, enhancing navigation and user experience.

## Tech Skills and Libraries

- **Next.js 14**: A powerful React framework that provides server-side rendering, automatic code splitting, and easy page routing, enabling the development of high-performance, SEO-friendly web applications. Version 14 brings enhanced features and optimizations for improved development experience and application performance.
- **App Router**: Next.js's built-in routing system that simplifies navigation within the application, providing seamless transitions between pages and enhancing the overall user experience. App Router efficiently manages client-side routing, allowing for dynamic loading of components and data as users navigate through the application.

## Quick Start

```bash
$ git clone <repository_url>
```

## Install

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev

# watch mode
$ npm run start

```
