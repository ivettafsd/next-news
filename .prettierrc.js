const plugins = [
  "@trivago/prettier-plugin-sort-imports",
  "prettier-plugin-classnames",
  // "prettier-plugin-merge",
];
module.exports = {
  plugins,
  importOrder: ["^@/(.*)$", "^[./]"],
  singleQuote: false,
  customAttributes: ["className"],
  customFunctions: ["classNames"],
};
