import Image from "next/image";
import { notFound } from "next/navigation";
import { getNewsItem } from "@/lib/news";

export default async function ImagePage({ params }) {
  const newsItemSlug = params.slug;
  const newsItem = await getNewsItem(newsItemSlug);

  if (!newsItem) {
    notFound();
  }

  return (
    <div className="fullscreen-image">
      <Image
        alt={newsItem.title}
        height={500}
        src={`/images/news/${newsItem.image}`}
        width={500}
      />
    </div>
  );
}
