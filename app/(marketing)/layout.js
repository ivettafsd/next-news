import "../globals.css";

export const metadata = {
  title: "Next News",
  description: "Stay informed with the latest news articles and updates.",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
