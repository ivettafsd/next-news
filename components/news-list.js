import React from "react";
import Image from "next/image";
import Link from "next/link";

function NewsList({ newsData }) {
  return (
    <ul className="news-list">
      {newsData?.map((news) => (
        <li key={news.id}>
          <Link href={`/news/${news.slug}`}>
            <Image
              alt={news.title}
              height={500}
              src={`/images/news/${news.image}`}
              width={500}
            />
            <span>{news.title}</span>
          </Link>
        </li>
      ))}
    </ul>
  );
}

export default NewsList;
